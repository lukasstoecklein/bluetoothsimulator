package com.example.bluetoothbroadcaster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.view.Menu;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
 
public class BluetoothActivity extends Activity implements OnClickListener{
 
	private static final int REQUEST_ENABLE_BT = 1;
	// Well known SPP UUID
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    
    public BluetoothAdapter mBluetoothAdapter;
    private BluetoothServerSocket mmServerSocket;
    
    private ToggleButton tb;
    private BluetoothAdapter bl;
 
    ListView listDevicesFound;
    Button btnMakeDiscoverable;
    Button btnScanDevice;
    TextView stateBluetooth;
    TextView connectedText;
    ArrayAdapter<String> btArrayAdapter;
    BluetoothSocket socket;
    ConnectedThread ct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        Ui();
    }
 
    public void Ui() {
 
        tb = (ToggleButton) findViewById(R.id.tb);
        tb.setOnClickListener(this);
        bl = BluetoothAdapter.getDefaultAdapter();
        // device doesnt support bluetooth, cant be helped
        if(bl == null){
        	Toast.makeText(getApplicationContext(), "No bluetooth device detected. That's embarrasing", Toast.LENGTH_LONG).show();
        }
        
        btnScanDevice = (Button)findViewById(R.id.scandevices);
        btnScanDevice.setOnClickListener(this);
        listDevicesFound = (ListView)findViewById(R.id.devicesfound);
        btArrayAdapter = new ArrayAdapter<String>(BluetoothActivity.this, android.R.layout.simple_list_item_1);
        listDevicesFound.setAdapter(btArrayAdapter);
        btnMakeDiscoverable = (Button) findViewById(R.id.btnMakeDiscoverable);
        btnMakeDiscoverable.setOnClickListener(this);
        
        connectedText = (TextView)findViewById(R.id.connectedText);
        CheckBlueToothState();
     //   btnScanDevice.setOnClickListener(btnScanDeviceOnClickListener);

        registerReceiver(ActionFoundReceiver, 
        new IntentFilter(BluetoothDevice.ACTION_FOUND));
    }
    private void CheckBlueToothState(){
        if (bl == null){
            stateBluetooth.setText("Bluetooth NOT support");
           }else{
            if (bl.isEnabled()){
             if(bl.isDiscovering()){
              //stateBluetooth.setText("Bluetooth is currently in device discovery process.");
             }else{
              //stateBluetooth.setText("Bluetooth is Enabled.");
              btnScanDevice.setEnabled(true);
             }
            }else{
             //stateBluetooth.setText("Bluetooth is NOT Enabled!");
             Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
           }
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
 
        getMenuInflater().inflate(R.menu.bluetooth, menu);
        return true;
    }
 
    @Override
    public void onClick(View v) {
 
        if (v == tb) {
 
            if ((tb.isChecked())) {
                try {
                	Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivity(enableIntent);
                    //bl.enable();
                    Toast.makeText(getApplicationContext(), "toggle button clicked", Toast.LENGTH_LONG).show();
 
                } catch (Exception e) {
 
                }
 
            } else {
                try {
 
                    bl.disable();
                } catch (Exception ee) {
 
                }
 
            }
 
        }
        
        if (v == btnScanDevice){
        
			
               
            String testString = "EC: 4.0, pH:0.2";
            ct.write(testString.getBytes());
	
	
                
//        	btArrayAdapter.clear();
//        	bl.startDiscovery();
//        	Toast.makeText(getApplicationContext(), "scan device clicked", Toast.LENGTH_LONG).show();
        }
        
        if (v == btnMakeDiscoverable){
        	Intent discoverableIntent = new
        	Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        	discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        	startActivity(discoverableIntent);
        	
        	

        

            AcceptThread();
            run();

        }
 
    }
    public void AcceptThread() {
        BluetoothServerSocket tmp = null;
        try {
            tmp = bl.listenUsingRfcommWithServiceRecord("MYYAPP", MY_UUID);

        } catch (IOException e) { }
        mmServerSocket = tmp;
    }

    public void run() {
        socket = null;
        while (true) {
            try {
                socket = mmServerSocket.accept();
                ct = new ConnectedThread(socket);
                ct.start();
                connectedText.setText("connected");
            } catch (IOException e) {
                break;
            }
            if (socket != null) {
         
                try {
                    mmServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
     // TODO Auto-generated method stub
     if(requestCode == REQUEST_ENABLE_BT){
    	 Toast.makeText(getApplicationContext(), "onactivityresult called. requesting enable bt", Toast.LENGTH_LONG).show();
    //	 CheckBlueToothState();
     }
    }
    
    private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver(){

    	  @Override
    	  public void onReceive(Context context, Intent intent) {
    	   // TODO Auto-generated method stub
    	   String action = intent.getAction();
    	   if(BluetoothDevice.ACTION_FOUND.equals(action)) {
    	             BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
    	             btArrayAdapter.add(device.getName() + "\n" + device.getAddress());
    	             btArrayAdapter.notifyDataSetChanged();
    	         }
    	  }
   };
   
   @Override
   protected void onDestroy() {
    // TODO Auto-generated method stub
    super.onDestroy();
    unregisterReceiver(ActionFoundReceiver);
   }
   private class ConnectedThread extends Thread {
	    private final BluetoothSocket mmSocket;
	    private final InputStream mmInStream;
	    private final OutputStream mmOutStream;

	    public ConnectedThread(BluetoothSocket socket) {
	        mmSocket = socket;
	        InputStream tmpIn = null;
	        OutputStream tmpOut = null;

	        // Get the input and output streams, using temp objects because
	        // member streams are final
	        try {
	            tmpIn = socket.getInputStream();
	            tmpOut = socket.getOutputStream();
	        } catch (IOException e) { }

	        mmInStream = tmpIn;
	        mmOutStream = tmpOut;
	    }

	    public void run() {
	        byte[] buffer = new byte[1024];  // buffer store for the stream
	        int bytes; // bytes returned from read()
	        Log.v("", "running ConnectedThread");
	        // Keep listening to the InputStream until an exception occurs
	        while (true) {
	            try {
	            	
	            	
	                // Read from the InputStream
	                bytes = mmInStream.read(buffer);
	                
	                // Send the obtained bytes to the UI Activity
	                BufferedReader r = new BufferedReader(new InputStreamReader(mmInStream));
	        		StringBuilder total = new StringBuilder();
	        		String line;
	        		while ((line = r.readLine()) != null) {
	        		    total.append(line);
	        		}
	        		Log.v("", line);
	        		
	        		
//	                mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
//	                        .sendToTarget();
	            } catch (IOException e) {
	                break;
	            }
	        }
	    }

	    /* Call this from the main Activity to send data to the remote device */
	    public void write(byte[] bytes) {
	        try {
	            mmOutStream.write(bytes);
	        } catch (IOException e) { }
	    }

	    /* Call this from the main Activity to shutdown the connection */
	    public void cancel() {
	        try {
	            mmSocket.close();
	        } catch (IOException e) { }
	    }
	}
}